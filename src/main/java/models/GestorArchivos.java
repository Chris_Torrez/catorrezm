/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author Sistemas-25
 */
public class GestorArchivos {
    

/**
 *
 * @author Christian
 */

    private String type;
    private String extension;
    private String content;
    private String fileName;

    public GestorArchivos() {
    }

    public GestorArchivos(String type, String extension, String content) {
        this.type = type;
        this.extension = extension;
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    
    
}

