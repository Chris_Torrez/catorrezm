/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import views.InternalShow;
import views.MainFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import models.GestorArchivos;
import views.EmployeeFrame;
import models.Empleado;

/**
 *
 * @author Sistemas-25
 */
public class MainController implements ActionListener {

    JFileChooser chooser;
    MainFrame frame;
    EmployeeFrame employFrame;

    public MainController() {
        frame = new MainFrame();
        chooser = new JFileChooser();
    }

    public MainController(MainFrame frame) {
        this.frame = frame;
    }

    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Seleccionar":
                showOpenFileDialog();
                break;
            case "Clear":
                clean();
                break;
            case "Save":
                showSaveFileDialog();
                break;
            case "Registrar":
                //showFileInForm();
            default:
                System.out.println("entra al case");

        }
    }

    private File showOpenFileDialog() {
        File file = null;
        if (chooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
            file = chooser.getSelectedFile();
            if (file != null) {
                try {
                    readFile(file);
                } catch (IOException ex) {
                    Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                JOptionPane.showMessageDialog(frame, "No se seleccionó archivo");
            }
        }
        return file;
    }

    private void readFile(File file) throws FileNotFoundException, IOException, ClassNotFoundException {

        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
        GestorArchivos myCustomFile = (GestorArchivos) ois.readObject();
        myCustomFile.setFileName(file.getName());
        //JOptionPane.showMessageDialog(frame, myCustomFile.getContent());
        showFileInForm(myCustomFile);

    }

    private void showFileInForm(GestorArchivos customfile) {
        EmployeeFrame n = new EmployeeFrame(customfile);
        frame.desktop.add(n);
        employFrame.add(n);
        n.setVisible(true);
        
        frame.desktop.getDesktopManager().maximizeFrame(n);
    }

    private void clean() {
        employFrame.firstNameTextField.setText("");
        employFrame.secondNameTextField.setText("");
        employFrame.lastName1TextField.setText("");
        employFrame.lastName2TextField.setText("");
        employFrame.birthDateTextField.setText("");

    }

    private void showSaveFileDialog() {
        File file;
        if (chooser.showSaveDialog(frame) == JFileChooser.APPROVE_OPTION) {
            file = chooser.getSelectedFile();
            if (file != null) {
                Empleado empleado = new Empleado(employFrame.firstNameTextField.getText(), employFrame.secondNameTextField.getText(),
                        employFrame.lastName1TextField.getText(), employFrame.lastName2TextField.getText(), employFrame.birthDateTextField.getText(),
                        employFrame.ageTextField.getText(),employFrame.aniosTextField.getText());
                       
                try {
                    writeFile(file, empleado);
                } catch (IOException ex) {
                    Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                JOptionPane.showMessageDialog(frame, "No se seleccionó archivo", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void writeFile(File file, Empleado empleado) throws IOException {
        try {
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file));
            w.writeObject(empleado);
            w.flush();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
